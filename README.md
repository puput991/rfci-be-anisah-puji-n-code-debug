# CodeDebugging

Code debugging built with NodeJs

Install terlebih dahulu module module yang dibutuhkan untuk development. Seperti npm install dotenv, npm install express, npm install axios, dan sebagainya.

1. Pada File app.js 
Ganti code ```markdown const config = require("./src/config"); ``` menjadi  ```markdown var config = require("./src/config/index");``` , karena jika kita hanya memanggil sampai dengan ```markdown ./src/config ``` belum bisa memanggil data-data dan fungsi-fungsi yang ada di delama file index.js

Ubah pemanggilan data dari index.js mengikuti dengan response data yang ada. Contohnya, dalam hal ini saya harus code ```markdown app.listen(config.port);```  menggubahnya menjadi  ```markdown app.listen(config.config.port); ```

2. Aktifkan file env , ubah menjadi file .env , kemudian isi data menggunakan new OAuth Apps pada github Anda. 
Sesuaikan isi datanya. 

3. Pada File index.js
Pindah code ```markdown const envFound = dotenv.config();``` di atas code ```markdown const config = port: process.env.PORT,clientId: process.env.CLIENT_ID,clientSecret: process.env.CLIENT_SECRET,oauthUrl: process.env.OAUTH_URL,apiUrl: process.env.API_URL,}``` agar isi dari variable config data terisi dari data .env


4. File authService.js
Ganti code ```markdown const config = require("../config");``` menjadi ```markdown const config = require("../config/index");``` (mengikuti dimana file module disimpan)
Ganti code ```markdown return `${config.oauthUrl}/authorize?client_id=${config.clientId}`;``` menjadi ```markdown return `${config.config.oauthUrl}/authorize?client_id=${config.config.clientId}`; ```
sesuaikan pemanggilan variable dengan response data yang ada
Tambahkan huruf `s` pada code ```markdown module.export = { redirectUri: redirectUri }``` menjadi ```markwdown module.exports = {redirectUri : redirectUri}```

5. File authCallbackService.js dan userInfoService.js
Pada file authCallbackService.js, saya menjadikan satu fungsi untuk pemanggilan API get user Github. Dikarenakan beberapa kali saya mencoba memisah pemanggilan API dengan file lagi tidak bisa (file userInfoService.js), maka saya gabung menjadi satu di file authCallbackService.js

