const axios = require("axios");
const UserServices = require("./userInfoService");
const config = require("../config/index");

function callback(req, res) {
  const body = {
    client_id: config.config.clientId,
    client_secret: config.config.clientSecret,
    code: req.query.code,
  };
  const options = { headers: { accept: "application/json" } };
  axios({
    method: 'post',
    url: `https://github.com/login/oauth/access_token?client_id=${body.client_id}&client_secret=${body.client_secret}&code=${body.code}`,
    headers: {
         accept: 'application/json'
    }
 }).then((response) => {
     axios({
        method: "get",
        url: `${config.config.apiUrl}/user`,
        headers: {
          Authorization: "token " + response.data.access_token,
        },
      }).then((response) => {
        res.json({
          data: {
            login: response.data.login,
            githubId: response.data.id,
            avatar: response.data.avatar_url,
            email: response.data.email,
            name: response.data.name,
            location: response.data.location,
          },
        });
      });
  }).catch((err) => res.status(500).json({ message: err.message }));
}

module.exports = {
  callback: callback,
};
